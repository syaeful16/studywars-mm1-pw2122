<?php 
    include 'connect.php';
    include 'users.php';

?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel=”icon” href=”img/ic_web.png”>
    <title>Login</title>

    <link rel="stylesheet" href="bootstrap-5.0.2-dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="css/style_log.css">
    <link rel="stylesheet" href="css/style_index.css">

    <!--font-->
    <style>
        @import url('https://fonts.googleapis.com/css2?family=Poppins:wght@400;500;600;700&display=swap');
        @import url('https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;500;700&display=swap');
    </style>

</head>
<body>
    <nav class="navbar navbar-expand-lg navbar-light ">
        <div class="container">
            <a class="navbar-brand fw-bolder" href="index.html">
                <img src="img/ic_web.png" alt="" width="40" height="40" style="margin-right: 10px; ">EST Cameras
            </a>
        </div>
    </nav>
    <div class="form-login">
        <div class="container d-flex justify-content-center align-items-center">
            <div class="row bg-white shadow">
                <div class="col pt-3 text-center border-bottom bg-active">
                    <p class="font-poppins-semi">Login</p>
                </div>
                <div class="col pt-3 text-center border-bottom">
                    <a href="signup.php" class="signup">
                    <p class="font-poppins-semi">Signup</p>
                    </a>
                </div>
                <form class="pt-5 mt-5 ps-5 pe-5" action="" method="POST">
                    <?php if(isset($status_login) == 'gagal') : ?>
                        <div class="alert alert-danger mb-3" role="alert">
                        Username atau password anda salah
                        </div>
                    <?php endif; ?>
                    <div class="mb-3 font-poppins-reg pb-3"> 
                        <input type="text" placeholder="No Telepon/Email" class="form-control pt-3 pb-3 " id="user" name="username">
                    </div>
                    <div class="mb-3 font-poppins-reg pb-3">
                        <input type="password" placeholder="Password" class="form-control pt-3 pb-3" id="pass" name="password">
                    </div>
                    <div class="mb-3 font-poppins-reg pb-5 pt-5 text-center"> 
                        <button type="submit" class="btn btn-login shadow pt-3 pb-3 font-poppins-semi" onclick="checkInputIsEmpty()" name="login">Log in</button>
                    </div>
                </form>
            </div>
            
        </div>
    </div>

    <script>
        function checkInputIsEmpty() {
            var usr = document.getElementById("user").value;
            var pass = document.getElementById("pass").value;

            if ( usr == "" && pass == "") {
                alert("Silahkan Isi terlebih dahulu");
            } else if ( usr == "admin" && pass == "admin"){
                alert("Login Berhasil");
            }
        }
        
    </script>
</body>
</html>