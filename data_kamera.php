<?php
    require('connect.php');

    $result = mysqli_query($conn, "SELECT * FROM data_kamera");

    while($data = mysqli_fetch_array($result)){
        $items[] = array(
            'id' => $data['id'],
            'gambar' => $data['gambar'],
            'nama_kamera' => $data['nama_kamera'],
            'max_sewa' => $data['max_sewa'],
            'spesifikasi' => $data['spesifikasi'],
            'harga' => $data['harga'],
            'username' => $data['username']
        );
    }

    $response = array(
        'status' => 'OK',
        'data' => $items
    );

    echo json_encode($response);
?>