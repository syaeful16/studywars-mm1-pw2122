<?php
     require('users.php');
     require('users_info.php');
 
     if (!isset($_SESSION['login'])) {
         header("Location: login.php");
         exit;
     }

     $userss = $_SESSION['username_user'];

     $result = mysqli_query($conn, "SELECT * FROM menunggu_konfirmasi WHERE pemilik='$userss'");

     function dataPenyewa($username) {
        global $conn;

        $data = mysqli_query($conn, "SELECT * FROM users WHERE username='$username'");
        $result = mysqli_fetch_assoc($data);
        return $result;
     }
     
     if(isset($_POST['tolak'])) {
         echo "<script>alert('tolak')</script>";
     }
?>

<!DOCTYPE html>
<html lang="en">
    <head>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="bootstrap-5.0.2-dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="css/style_pelanggan.css">
    <link rel=”icon” href=”img/ic_web.png”>
    <title>Menunggu Konfirmasi</title>
    <!-- font -->
    <style>
        @import url('https://fonts.googleapis.com/css2?family=Poppins:wght@400;500;600;700&display=swap');
        @import url('https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;500;700&display=swap');
    </style>
    
    </head>
    <body>
        <nav class="navbar navbar-expand-lg navbar-light shadow">
            <div class="container">
                <a class="navbar-brand fw-bolder" href="main.php">
                    <img src="img/ic_web.png" alt="" width="40" height="40" style="margin-right: 10px; ">EST Cameras
                </a>
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse justify-content-between text-center" id="navbarNavAltMarkup">
                    <div class="navbar-nav ms-auto nav-barbar">
                        <h6 class="justify-content-center align-self-center"><?php echo $row['nama'];?></h6>
                        <div class="dropdown">
                            <button class="btn dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                              <?php if ($row['gambar'] == "") : ?>
                                <img class="img-user" style="width:35px; height:35px; border-radius:50%; object-fit:cover;" src="img/ic_user.png" class="img-user" alt="" class="dropdown-toggle" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                            <?php else : ?>
                                <img class="img-user" style="width:35px; height:35px; border-radius:50%; object-fit:cover;" src="uploads/<?php echo $row['gambar'];?>" class="img-user" alt="" class="dropdown-toggle" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                            <?php endif;?>
                            </button>
                            <ul class="dropdown-menu p-4 font-poppins-reg" aria-labelledby="dropdownMenuButton1">
                              <li><a class="dropdown-item" href="editProfile.php">Edit Profile</a></li>
                              <li><a class="dropdown-item" href="inputData.php">Sewakan Kamera</a></li>
                              <li><a class="dropdown-item" href="menungguKonfirmasi.php">Menunggu Konfirmasi</a></li>
                              <li><a class="dropdown-item" href="pelangganSaya.php">Pelanggan Saya</a></li>
                              <li><a class="dropdown-item btn-logout-dwn" href="logout.php">Log Out</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </nav>
    <div class="container const-width mt-5">
        <h4 class="font-poppins-sb mb-5">Menunggu Konfirmasi</h4>
        <?php while($row = mysqli_fetch_assoc($result)) : ?>
        <div class="pelanggan shadow p-4 mb-4">
            <div class="row">
                <div class="col float-start border-end">                    
                    <h5 class="font-poppins-sb"><?php echo dataPenyewa($row['penyewa'])['nama']; ?></h5>                    
                    <p class="no-tlpn">Lama Sewa : <?php echo $row['jumlah_hari']; ?> Hari</p>
                </div>
                <div class="col border-end">
                    <p class="no-tlpn"><span><svg xmlns="http://www.w3.org/2000/svg" width="9.8" height="14" viewBox="0 0 9.8 14" class="me-2">
                        <path id="Icon_material-location-on" data-name="Icon material-location-on" d="M12.4,3A4.9,4.9,0,0,0,7.5,7.9c0,3.675,4.9,9.1,4.9,9.1s4.9-5.425,4.9-9.1A4.9,4.9,0,0,0,12.4,3Zm0,6.65A1.75,1.75,0,1,1,14.15,7.9,1.751,1.751,0,0,1,12.4,9.65Z" transform="translate(-7.5 -3)" fill="#aaa"/>
                      </svg></span><?php echo dataPenyewa($row['penyewa'])['kota'].", ".dataPenyewa($row['penyewa'])['provinsi']; ?></p>
                </div>
                <div class="col float-end d-flex justify-content-md-end align-items-center gap-2">             
                    <a href="tolak.php?id=<?= $row['id'] ?>"><button type="submit" class="btn btn-outline-danger" name="tolak">Tolak</button></a>
                    <a href="konfirmasi.php?id=<?= $row['id'] ?>&usernamePenyewa=<?= $row['penyewa']; ?>&nama=<?= dataPenyewa($row['penyewa'])['nama'];?>&bayar=<?= $row['total_bayar'];?>&provinsi=<?= dataPenyewa($row['penyewa'])['provinsi'];?>&hari=<?= $row['jumlah_hari'];?>&pemilik=<?= $row['pemilik'];?>"><button type="submit" class="btn btn-success" name="konfirm">Konfirmasi</button></a>
                </div>
            </div>
        </div>
        <?php endwhile; ?>
    </div>    
    <script type="text/javascript" src="bootstrap-5.0.2-dist/js/bootstrap.min.js"></script>
</body>
</html>