<?php
    session_start();

    include 'connect.php';

    if (isset($_POST['regis'])) {
        $nama = $_POST['nama'];
        $ttl = $_POST['ttl'];
        $notlpn = $_POST['notlpn'];
        $email = $_POST['email'];
        $user = $_POST['username'];
        $pass = $_POST['password'];
        $cpass = $_POST['cpassword'];
        $prov = $_POST['provinsi'];
        $kota = $_POST['kota'];
        $alamat = $_POST['alamat'];

        $cek_username = mysqli_query($conn, "SELECT username FROM users WHERE username='$user'");

        if( mysqli_fetch_assoc($cek_username)) {
            $status_username_error = 'error';
            return false;
        }

        if($pass != $cpass) {
            echo "<script>alert('Password Tidak Sesuai')</script>";
            return false;
        }

        $pass = password_hash($pass, PASSWORD_DEFAULT);

        mysqli_query($conn, "INSERT INTO users VALUES('$nama', '', '$ttl', '$notlpn', '$email', '$user', '$pass', '$prov', '$kota', '$alamat')");

        if(mysqli_affected_rows($conn) > 0){
            $status_tabel = 'success';
        }
    }

    if (isset($_POST['login'])) {
        $login_user = $_POST['username'];
        $login_password = $_POST['password'];

        $log = mysqli_query($conn, "SELECT * FROM users WHERE username='$login_user'");

        if (mysqli_num_rows($log) === 1) {
            $row = mysqli_fetch_assoc($log);
            
            if (password_verify($login_password, $row['passwords'])) {
                $_SESSION['login'] = true;
                $_SESSION['username_user'] = $login_user;
                header("Location: main.php");
                exit;
            } else {
                $status_login = 'gagal';
            }
        } else {
            $status_login = 'gagal';
        }
    }
?>