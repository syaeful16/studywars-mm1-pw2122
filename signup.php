<?php
    include 'connect.php';
    include 'users.php';
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel=”icon” href=”img/ic_web.png”>
    <title>Sign Up</title>

    <link rel="stylesheet" href="bootstrap-5.0.2-dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="css/style_log.css">
    <link rel="stylesheet" href="css/style_index.css">

    <!--font-->
    <style>
        @import url('https://fonts.googleapis.com/css2?family=Poppins:wght@400;500;600;700&display=swap');
        @import url('https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;500;700&display=swap');
    </style>

</head>
<body>
    <nav class="navbar navbar-expand-lg navbar-light ">
        <div class="container">
            <a class="navbar-brand fw-bolder" href="index.html">
                <img src="img/ic_web.png" alt="" width="40" height="40" style="margin-right: 10px; ">EST Cameras
            </a>
        </div>
    </nav>
    <div class="form-login mb-5">
        <div class="container d-flex justify-content-center align-items-center">
            <div class="row bg-white shadow">
                <div class="col pt-3 text-center border-bottom ">
                    <a href="login.php" class="signup">
                        <p class="font-poppins-semi">Login</p>
                    </a>
                </div>
                <div class="col pt-3 text-center border-bottom bg-active">
                    <p class="font-poppins-semi">Signup</p>
                </div>
                <form class="form mt-5 pt-4 ps-3 pe-3" action="" method="POST">
                    <?php if(isset($status_username_error) == 'error') : ?>
                        <div class="alert alert-danger ms-5 me-5" role="alert">
                        Username sudah terdaftar
                        </div>
                    <?php endif; ?>
                    <?php if(isset($status_tabel) == 'success') : ?>
                        <div class="alert alert-success ms-5 me-5" role="alert">
                        Berhasil daftar, siallahkan login
                        </div>
                    <?php endif; ?>
                    <div class="mb-3 font-poppins-reg ms-5 me-5"> 
                        <input type="text" placeholder="Nama Lengkap" class="form-control pt-3 pb-3" id="nama" name="nama">
                    </div>
                    <div class="mb-3 font-poppins-reg ms-5 me-5" id="datepicker">
                        <input type="date" placeholder="Tanggal Lahir" class="form-control pt-3 pb-3" id="ttl" name="ttl">
                    </div>
                    <div class="mb-3 font-poppins-reg ms-5 me-5"> 
                        <input type="text" placeholder="No Telepon" class="form-control pt-3 pb-3" id="notlpn" name="notlpn">
                    </div>
                    <div class="mb-3 font-poppins-reg ms-5 me-5">
                        <input type="email" placeholder="Email" class="form-control pt-3 pb-3" id="email" name="email"">
                    </div>
                    <div class="mb-3 font-poppins-reg ms-5 me-5">
                        <input type="text" placeholder="Username" class="form-control pt-3 pb-3" id="username" name="username">
                    </div>
                    <div class="mb-3 font-poppins-reg ms-5 me-5"> 
                        <input type="password" placeholder="Password" class="form-control pt-3 pb-3" id="password" name="password">
                    </div>
                    <div class="mb-3 font-poppins-reg ms-5 me-5">
                        <input type="password" placeholder="Ulangi Password" class="form-control pt-3 pb-3" id="cpassword" name="cpassword">
                    </div>
                    <div class="mb-3 font-poppins-reg ms-5 me-5">
                        <input type="text" placeholder="Provinsi" class="form-control pt-3 pb-3" id="provinsi" name="provinsi">
                    </div>
                    <div class="mb-3 font-poppins-reg ms-5 me-5"> 
                        <input type="text" placeholder="Kabupaten / Kota" class="form-control pt-3 pb-3" id="kota" name="kota">
                    </div>
                    <div class="form-group font-poppins-reg ms-5 me-5">
                        <textarea class="form-control" placeholder="Alamat Lengkap" id="alamat" rows="3" name="alamat"></textarea>
                    </div>
                    <div class="mb-3 font-poppins-reg ms-5 me-5 pb-5 pt-5 text-center"> 
                        <button type="submit" class="btn btn-login shadow pt-3 pb-3 font-poppins-semi" name="regis">Signup</button>
                    </div>
                </form>
            </div>
            
        </div>
    </div>
</body>
</html>