<?php
    
    require('users_info.php');

    if (isset($_POST['tambahkan'])) {
        $namaKamera = $_POST['nama_kamera'];
        $makshari = $_POST['hari'];
        $spesifikasi = $_POST['spesifikasi'];
        $harga = $_POST['harga'];

        $gambar = uploadGambar();
        if ( !$gambar) {
            return false;
        }

        mysqli_query($conn, "INSERT INTO data_kamera VALUES ('', '$gambar', '$namaKamera', '$makshari', '$spesifikasi', '$harga', '$nama_pengguna')");

        if(mysqli_affected_rows($conn) > 0){
            echo "<script>alert('Berhsil ditambahkan');</script>";
        }
    }

    function uploadGambar() {
        $namaFile = $_FILES['input_img']['name'];
        $ukuranFile = $_FILES['input_img']['size'];
        $error = $_FILES['input_img']['error'];
        $tmpName = $_FILES['input_img']['tmp_name'];

        if ($error === 4){
            echo "<script>alert('pilih gambar terlebih dahulu'); document.location.href = 'inputData.php';</script>";
            return false;
        }

        $ektensiGambarValid = ['jpg', 'jpeg', 'png'];
        $ektensiGambar = explode('.', $namaFile);
        $ektensiGambar = strtolower(end($ektensiGambar));

        if(!in_array($ektensiGambar, $ektensiGambarValid)) {
            echo "<script>alert('Yang anda upload bukan gambar!'); document.location.href = 'inputData.php';</script>";
            return false;
        }

        if ( $ukuranFile > 1048576) {
            echo "<script>alert('Ukuran gambar terlalu besar!'); document.location.href = 'inputData.php';</script>";
            return false;
        }

        //Buat nama file baru
        $namaFileBaru = uniqid();
        $namaFileBaru .= '.';
        $namaFileBaru .= $ektensiGambar;

        move_uploaded_file($tmpName, 'uploads/kamera/' . $namaFileBaru);
        return $namaFileBaru;
    }

?>