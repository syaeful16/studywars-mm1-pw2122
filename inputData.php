<?php
    require('users.php');
    require('tambahdata.php');

    if (!isset($_SESSION['login'])) {
        header("Location: login.php");
        exit;
    }
?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel=”icon” href=”img/ic_web.png”>
        <title>Sewakan Kamera - Input data</title>
    
        <link rel="stylesheet" href="bootstrap-5.0.2-dist/css/bootstrap.min.css">
        <link rel="stylesheet" href="css/style_inputData.css">
    
        <!--font-->
        <style>
            @import url('https://fonts.googleapis.com/css2?family=Poppins:wght@400;500;600;700&display=swap');
            @import url('https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;500;700&display=swap');
        </style>
    </head>
    <body>
        <nav class="navbar navbar-expand-lg navbar-light shadow">
            <div class="container">
                <a class="navbar-brand fw-bolder" href="main.php">
                    <img src="img/ic_web.png" alt="" width="40" height="40" style="margin-right: 10px; ">EST Cameras
                </a>
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse justify-content-between text-center" id="navbarNavAltMarkup">
                    <div class="navbar-nav ms-auto nav-barbar">
                        <h6 class="justify-content-center align-self-center"><?php echo $row['nama'];?></h6>
                        <div class="dropdown">
                            <button class="btn dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                            <?php if ($row['gambar'] == "") : ?>
                                <img class="img-user" style="width:35px; height:35px; border-radius:50%; object-fit:cover;" src="img/ic_user.png" class="img-user" alt="" class="dropdown-toggle" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                            <?php else : ?>
                                <img class="img-user" style="width:35px; height:35px; border-radius:50%; object-fit:cover;" src="uploads/<?php echo $row['gambar'];?>" class="img-user" alt="" class="dropdown-toggle" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                            <?php endif;?>
                            </button>
                            <ul class="dropdown-menu p-4 font-poppins-reg" aria-labelledby="dropdownMenuButton1">
                              <li><a class="dropdown-item" href="editProfile.php">Edit Profile</a></li>
                              <li><a class="dropdown-item" href="inputData.php">Sewakan Kamera</a></li>
                              <li><a class="dropdown-item" href="menungguKonfirmasi.php">Menunggu Konfirmasi</a></li>
                              <li><a class="dropdown-item" href="pelangganSaya.php">Pelanggan Saya</a></li>
                              <li><a class="dropdown-item btn-logout-dwn" href="logout.php">Log Out</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </nav>
        <form action="" method="post" enctype="multipart/form-data">
            <div class="container cont mt-5">
                <h2 class="mb-5">Sewakan Kamera</h2>
                <div class="row pb-4">
                    <div class="col">
                        <div id="display_images">
                        </div>
                    </div>
                    <div class="col d-flex align-items-center">
                        <input type="file" id="image_input" name="input_img" accept="image/png, image/jpg">
                    </div>
                </div>
                <hr>
                <div class="row pt-3">
                    <div class="col">
                        <h3>Informasi</h3>
                        <p>Anda dapat mengubah Email dan no telepon</p>
                    </div>
                    <div class="col">
                        <div class="mb-3">
                            <label for="kodeBuku" class="form-label">Nama Kamera</label>
                            <input type="text" class="form-control" id="kodeBuku" name="nama_kamera">
                        </div>
                        <div class="mb-3">
                            <label for="kodeBuku" class="form-label">Harga Sewa Perhari</label>
                            <input type="text" class="form-control" id="kodeBuku" name="harga">
                        </div>
                        <div class="mb-3">
                            <label for="kodeBuku" class="form-label">Maks. Sewa (hari)</label>
                            <input type="text" class="form-control" id="kodeBuku" name="hari">
                        </div>
                        <div class="mb-3">
                            <label for="exampleFormControlTextarea1" class="form-label">Spesifikasi / Deskripsi</label>
                            <textarea class="form-control" id="exampleFormControlTextarea1" rows="6" name="spesifikasi"></textarea>
                        </div>
                    </div>
                </div>
                <button type="submit" class="btn btn-dark mt-4 mb-5" name="tambahkan">Simpan</button>
            </div>
        </form>
        <script type="text/javascript" src="bootstrap-5.0.2-dist/js/bootstrap.min.js"></script>
        <script type="text/javascript">
            const image_input = document.querySelector("#image_input");
            var uploaded_image;
    
            image_input.addEventListener('change', function() {
                const reader = new FileReader();
                console.log(reader);
                reader.addEventListener('load', () => {
                    uploaded_image = reader.result;
                    document.querySelector("#display_images").style.backgroundImage = `url(${uploaded_image})`;
                });
                reader.readAsDataURL(this.files[0]);
            });
        </script>
    </body>
</html>