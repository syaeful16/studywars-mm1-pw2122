<?php
    require('users.php');
    require('users_info.php');

    if (!isset($_SESSION['login'])) {
        header("Location: login.php");
        exit;
    }

    $usernameSession = $_SESSION['username_user'];

    $tampilkan_data = query("SELECT * FROM data_kamera");

    function dataFk($username) {
        global $conn;
        $dataFk = mysqli_query($conn, "SELECT users.* FROM users INNER JOIN data_kamera ON users.username = data_kamera.username WHERE data_kamera.username='$username'");

        $viewsDataFk = mysqli_fetch_assoc($dataFk);
        return $viewsDataFk;
    }

    if (isset($_POST['cari'])) {
        $tampilkan_data = cari($_POST['keyword']);
    }

    if (isset($_POST['filter'])) {
        $lokasi = $_POST['lokasi'];
        $kategori = $_POST['kategori'];

        $tampilkan_data = filter($lokasi, $kategori);
    }

    if (isset($_POST['show'])) {
        $tampilkan_data = showMyData($usernameSession);
    }

    function query($query) {
        global $conn;
        $result = mysqli_query($conn, $query);
        $rows = [];
        while ($row = mysqli_fetch_assoc($result)) {
            $rows[] = $row;
        }
        return $rows;
    }

    function cari($keyword) {
        $query = "SELECT * FROM data_kamera WHERE nama_kamera LIKE '%$keyword%'";
        return query($query);
    }

    function filter($kota, $nama) {
        $query = "SELECT data_kamera.* FROM users INNER JOIN data_kamera ON users.username = data_kamera.username WHERE users.kota='$kota' AND data_kamera.nama_kamera LIKE '%$nama%'";
        return query($query);
    }

    function showMyData($username) {
        $query = "SELECT * FROM data_kamera WHERE username='$username'";
        return query($query);
    }

    $lokasi = mysqli_query($conn, "SELECT DISTINCT kota FROM users");

?>


<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel=”icon” href=”img/ic_web.png”>
        <title>Beranda</title>
    
        <link rel="stylesheet" href="bootstrap-5.0.2-dist/css/bootstrap.min.css">
        <link rel="stylesheet" href="css/style_main.css">
    
        <!--font-->
        <style>
            @import url('https://fonts.googleapis.com/css2?family=Poppins:wght@400;500;600;700&display=swap');
            @import url('https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;500;700&display=swap');
        </style>
    </head>
    <body>
        <nav class="navbar navbar-expand-lg navbar-light shadow">
            <div class="container">
                <a class="navbar-brand fw-bolder" href="#">
                    <img src="img/ic_web.png" alt="" width="40" height="40" style="margin-right: 10px; ">EST Cameras
                </a>
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse justify-content-between text-center" id="navbarNavAltMarkup">
                    <div class="navbar-nav ms-auto nav-barbar">
                        <h6 class="justify-content-center align-self-center"><?php echo $row['nama'];?></h6>
                        <div class="dropdown">
                            <button class="btn dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                            <?php if ($row['gambar'] == "") : ?>
                                <img class="img-user" style="width:35px; height:35px; border-radius:50%; object-fit:cover;" src="img/ic_user.png" class="img-user" alt="" class="dropdown-toggle" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                            <?php else : ?>
                                <img class="img-user" style="width:35px; height:35px; border-radius:50%; object-fit:cover;" src="uploads/<?php echo $row['gambar'];?>" class="img-user" alt="" class="dropdown-toggle" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                            <?php endif;?>
                            </button>
                            <ul class="dropdown-menu p-4 font-poppins-reg" aria-labelledby="dropdownMenuButton1">
                              <li><a class="dropdown-item font-nav" href="editProfile.php">Edit Profile</a></li>
                              <li><a class="dropdown-item font-nav" href="inputData.php">Sewakan Kamera</a></li>
                              <li><a class="dropdown-item font-nav" href="menungguKonfirmasi.php">Menunggu Konfirmasi</a></li>
                              <li><a class="dropdown-item font-nav" href="pelangganSaya.php">Pelanggan Saya</a></li>
                              <li><a class="dropdown-item font-nav btn-logout-dwn" href="logout.php">Log Out</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </nav>
        <div class="container mt-5">
            <div class="row">
                <div class="col-3">
                </div>
                <div class="col">
                    <form action="" method="post">
                        <form action="" method="post">
                            <div class="input-group mb-3">
                                <input type="text" class="form-control pt-2 pb-2" placeholder="Cari Kamera" aria-label="Recipient's username" aria-describedby="button-addon2" id="keyword" name="keyword">
                                <button id="btnCari" name="cari" class="btn btn-dark " type="submit" id="button-addon2">Cari</button> 
                            </div>
                        </form>
                    </form>
                </div>
            </div>
        </div>
        <div class="container margin mt-5">
            <div class="row">
                <div class="col-3 border-left-col">
                    <form action="" method="post">
                        <p class="mb-4 font-poppins-med font-filter"><span class="pe-2"><img src="img/icon/ic_filter.png" alt=""></span>filter</p>
                        <label for="inputGroupSelect01" class="form-label font-poppins-med font-filter">Lokasi</label>
                        <div class="input-group mb-3">
                            <select class="form-select font-poppins-reg select-lokasi pt-2 pb-2" id="inputGroupSelect01" name="lokasi">
                            <option selected>Choose...</option>
                            <?php while($value = mysqli_fetch_assoc($lokasi)) : ?>
                            <option value="<?php echo $value['kota']; ?>"><?php echo $value['kota']; ?></option>
                            <?php endwhile; ?>
                            </select>
                        </div>
                        <label for="inputGroupSelect01" class="form-label font-poppins-med font-filter mt-3">Kamera</label>
                        <div class="font-poppins-reg font-radio">
                            <div class="form-check">
                                <input class="form-check-input" type="radio" id="flexRadioDefault1" name="kategori" value="cannon">
                                <label class="form-check-label" for="flexRadioDefault1">
                                Cannon
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" id="flexRadioDefault1" name="kategori" value="sony">
                                <label class="form-check-label" for="flexRadioDefault1">
                                Sony
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" id="flexRadioDefault1" name="kategori" value="epson">
                                <label class="form-check-label" for="flexRadioDefault1">
                                Epson
                                </label>
                            </div>
                        </div>
                        <button type="submit" class="btn-filter mt-5 pt-2 pb-2" name="filter">Terapkan</button>
                    </form>
                    <form action="" method="post" class="mt-4">
                        <button type="submit" class="btn btn-link" style="color:black;" name="show">Tampilkan data sewa saya</button>
                    </form>
                </div>
                <div class="col">
                    <div id="data">
                    <?php foreach($tampilkan_data as $row) : ?>
                        <a style="text-decoration: none; color: black;" href="review.php?id=<?= $row['id']; ?>& nama_kamera=<?= $row['nama_kamera'];?>">
                            <div class="card-camera">
                                <img src="uploads/kamera/<?php echo $row["gambar"];?>" alt="" style="object-fit: cover;">
                                <div class="info-item p-3">
                                    <h5 class="font-kamera"><?php echo $row["nama_kamera"]?></h5>
                                    <p class="font-nav"><span class="pe-2"><svg xmlns="http://www.w3.org/2000/svg" width="9.8" height="14" viewBox="0 0 9.8 14">
                                        <path id="Icon_material-location-on" data-name="Icon material-location-on" d="M12.4,3A4.9,4.9,0,0,0,7.5,7.9c0,3.675,4.9,9.1,4.9,9.1s4.9-5.425,4.9-9.1A4.9,4.9,0,0,0,12.4,3Zm0,6.65A1.75,1.75,0,1,1,14.15,7.9,1.751,1.751,0,0,1,12.4,9.65Z" transform="translate(-7.5 -3)" fill="#aaa"/>
                                    </svg></span><?php echo dataFk($row["username"])['kota'].', '.dataFk($row["username"])['provinsi'] ?></p>
                                </div>
                            </div>
                        </a>
                    <?php endforeach; ?>
                    </div>
                </div>
            </div>
        </div>
        <script type="text/javascript" src="bootstrap-5.0.2-dist/js/bootstrap.min.js"></script>
    </body>
</html>
