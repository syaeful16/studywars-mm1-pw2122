<?php
    require('users.php');
    require('users_info.php');

    if (!isset($_SESSION['login'])) {
        header("Location: login.php");
        exit;
    }

    $usernameSession = $_SESSION['username_user'];

    $id_data = $_GET['id'];

    $get_data_camera = mysqli_query($conn, "SELECT * FROM data_kamera WHERE id='$id_data'");
    
    $rowValueData = mysqli_fetch_assoc($get_data_camera);

    $data_users = $rowValueData['username'];

    $get_data_users = mysqli_query($conn, "SELECT * FROM users WHERE username='$data_users'");

    $rowValueDataUsers = mysqli_fetch_assoc($get_data_users);

    if (isset($_POST['konfirmasi'])) {
        $penyewa = $_POST['penyewa'];
        $jumlah_hari = $_POST['jmlHari'];
        $total_bayar = $_POST['totalBayar'];
        $pemilik = $_POST['pemilik'];

        mysqli_query($conn, "INSERT INTO menunggu_konfirmasi VALUES('', '$penyewa', '$jumlah_hari', '$total_bayar', '$pemilik')");

    }

    $dataRekomen = query("SELECT * FROM data_kamera ORDER BY RAND() LIMIT 4");

    function query($query) {
        global $conn;
        $result = mysqli_query($conn, $query);
        $rows = [];
        while ($row = mysqli_fetch_assoc($result)) {
            $rows[] = $row;
        }
        return $rows;
    }
?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel=”icon” href=”img/ic_web.png”>
        <title>Review Camera</title>
    
        <link rel="stylesheet" href="bootstrap-5.0.2-dist/css/bootstrap.min.css">
        <link rel="stylesheet" href="css/style_review.css">
    
        <!--font-->
        <style>
            @import url('https://fonts.googleapis.com/css2?family=Poppins:wght@400;500;600;700&display=swap');
            @import url('https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;500;700&display=swap');
        </style>
    </head>
    <body>
        <nav class="navbar navbar-expand-lg navbar-light shadow">
            <div class="container">
                <a class="navbar-brand fw-bolder" href="main.php">
                    <img src="img/ic_web.png" alt="" width="40" height="40" style="margin-right: 10px; ">EST Cameras
                </a>
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse justify-content-between text-center" id="navbarNavAltMarkup">
                    <div class="navbar-nav ms-auto nav-barbar">
                        <h6 class="justify-content-center align-self-center"><?php echo $row['nama'];?></h6>
                        <div class="dropdown">
                            <button class="btn dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                              <?php if ($row['gambar'] == "") : ?>
                                <img class="img-user" style="width:35px; height:35px; border-radius:50%; object-fit:cover;" src="img/ic_user.png" class="img-user" alt="" class="dropdown-toggle" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                            <?php else : ?>
                                <img class="img-user" style="width:35px; height:35px; border-radius:50%; object-fit:cover;" src="uploads/<?php echo $row['gambar'];?>" class="img-user" alt="" class="dropdown-toggle" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                            <?php endif;?>
                            </button>
                            <ul class="dropdown-menu p-4 font-poppins-reg font-nav" aria-labelledby="dropdownMenuButton1">
                              <li><a class="dropdown-item" href="editProfile.php">Edit Profile</a></li>
                              <li><a class="dropdown-item" href="inputData.php">Sewakan Kamera</a></li>
                              <li><a class="dropdown-item" href="menungguKonfirmasi.php">Menunggu Konfirmasi</a></li>
                              <li><a class="dropdown-item" href="#">Pelanggan Saya</a></li>
                              <li><a class="dropdown-item btn-logout-dwn" href="logout.php">Log Out</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </nav>
        <div class="container cont-width mt-5 mb-5">
            <p><a href="main.php" style="text-decoration: underline; color:black;">Beranda</a> / <?php echo $rowValueData ['nama_kamera'];?></p>
            <div class="card-img-cam shadow mt-3">
                <div class="row">
                    <div class="col">
                        <img src="uploads/kamera/<?php echo $rowValueData['gambar'];?>" alt="" class="img-cam-view">
                    </div>
                    <div class="col-5 ps-3 pe-5">
                        <h3 class="font-poppins-med name-camera"><?php echo $rowValueData ['nama_kamera'];?></h3>
                        <p class="font-poppins-reg txt-alamat"><span><svg xmlns="http://www.w3.org/2000/svg" width="9.8" height="14" viewBox="0 0 9.8 14" class="me-2">
                            <path id="Icon_material-location-on" data-name="Icon material-location-on" d="M12.4,3A4.9,4.9,0,0,0,7.5,7.9c0,3.675,4.9,9.1,4.9,9.1s4.9-5.425,4.9-9.1A4.9,4.9,0,0,0,12.4,3Zm0,6.65A1.75,1.75,0,1,1,14.15,7.9,1.751,1.751,0,0,1,12.4,9.65Z" transform="translate(-7.5 -3)" fill="#aaa"/>
                          </svg></span><?php echo $rowValueDataUsers['kota'].", ".$rowValueDataUsers['provinsi'];?></p>

                        <h5 class="font-poppins-med title-desc">Deskripsi</h5>
                        <p class="font-poppins-reg txt-desc"><?php echo $rowValueData['spesifikasi']?></p>
                        <h5 class="font-poppins-med title-desc">Maks. Penyewaan</h5>
                        <p class="font-poppins-reg txt-desc"><?php echo $rowValueData['max_sewa'];?> hari</p>
                        <h5 class="font-poppins-med title-desc">Harga Sewa Perhari</h5>
                        <p class="font-poppins-reg txt-desc">Rp <?php echo number_format($rowValueData ['harga'],0,',','.');?></p>
                        <div class="row mt-5">
                            <?php if ($usernameSession == $rowValueData['username'] ) : ?>
                            <div class="col">
                                <a href="editData.php?id=<?= $rowValueData['id'] ?>"><button type="submit" class="btn btn-card-view btn-danger mb-4">Edit</button></a>
                            </div>
                            <?php endif; ?>
                            <div class="col">
                                <button type="button" class="btn btn-card-view btn-dark mb-4" id="btn-sewa">Sewa</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <h4 class="font-poppins-med mt-5">Rekomendasi</h4>
            <div class="rekomen">
                <?php foreach($dataRekomen as $row) : ?>
                <a href="review.php?id=<?= $row['id']; ?>& nama_kamera=<?= $row['nama_kamera'];?>">
                    <div class="card-camera">
                        <img src="uploads/kamera/<?php echo $row["gambar"];?>" alt="" style="height: 160px; object-fit:contain;">
                        <div class="info-item p-3">
                            <h5><?php echo $row["nama_kamera"];?></h5>
                            <p><span class="pe-2"><svg xmlns="http://www.w3.org/2000/svg" width="9.8" height="14" viewBox="0 0 9.8 14">
                                <path id="Icon_material-location-on" data-name="Icon material-location-on" d="M12.4,3A4.9,4.9,0,0,0,7.5,7.9c0,3.675,4.9,9.1,4.9,9.1s4.9-5.425,4.9-9.1A4.9,4.9,0,0,0,12.4,3Zm0,6.65A1.75,1.75,0,1,1,14.15,7.9,1.751,1.751,0,0,1,12.4,9.65Z" transform="translate(-7.5 -3)" fill="#aaa"/>
                              </svg></span>Purbalingga</p>
                        </div>
                    </div>
                </a>
                <?php endforeach ?>
            </div>
        </div>
        <div class="bg-modal">
            <div class="model-content p-5">
                <div class="close">+</div>
                <h3 class="text-center mb-5 font-poppins-med">Konfirmasi Sewa</h3>
                <p id="hargaPerhari" style="display: none;"><?php echo $rowValueData ['harga'];?></p>
                <p id="maxHari" style="display: none;"><?php echo $rowValueData ['max_sewa'];?></p>
                <form action="" method="post">
                    <div class="mb-4">
                        <label for="kodeBuku" class="form-label">Total Hari</label>
                        <input type="text" class="form-control" id="jmlHari" name="jmlHari" oninput="total(this.value);">
                        <p id="cekHari" style="position: absolute; font-size:14px; color: red;"></p>
                    </div>
                    <div class="mb-5">
                        <label for="kodeBuku" class="form-label">Total Bayar</label>
                        <h4 id="bayar">Rp 0</h4>
                        <input type="text" class="form-control" id="totalBayar" name="totalBayar" style="display: none;">
                        <input type="text" class="form-control" name="penyewa" style="display: none;" value="<?php echo $_SESSION['username_user']; ?>">
                        <input type="text" class="form-control" name="pemilik" style="display: none;" value="<?php echo $rowValueData ['username']; ?>">
                    </div>
                    <button type="submit" class="btn btn-card-view btn-dark font-poppins-med" name="konfirmasi">Konfirmasi</button>             
                </form>
            </div>                           
        </div>                     

        <script type="text/javascript" src="bootstrap-5.0.2-dist/js/bootstrap.min.js"></script>

    </body>
    <script>
        document.getElementById('btn-sewa').addEventListener('click', function(){
            document.querySelector('.bg-modal').style.display = 'flex';
        });

        document.querySelector('.close').addEventListener('click', function(){
            document.querySelector('.bg-modal').style.display = 'none';
        });

        function total(value) {
            var harga = document.getElementById('hargaPerhari').innerHTML;
            var maxHari = document.getElementById('maxHari').innerHTML;
            var total = value * harga;

            console.log(value);
            if ( value > 0 && value <= maxHari) {
                document.getElementById('cekHari').innerHTML = "";
                document.getElementById('bayar').innerHTML = "Rp " + total;
                document.getElementById('totalBayar').value = total;
            } else if(value > 3){
                document.getElementById('cekHari').innerHTML = "Melebihi batas hari sewa";
            }
        }

        
    </script>
</html>