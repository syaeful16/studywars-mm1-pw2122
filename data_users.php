<?php
    require('connect.php');

    $result = mysqli_query($conn, "SELECT * FROM users");

    while($data = mysqli_fetch_array($result)){
        $items[] = array(
            'nama' => $data['nama'],
            'gambar' => $data['gambar'],
            'tanggal_lahir' => $data['tanggal_lahir'],
            'no_telepon' => $data['no_telepon'],
            'email' => $data['email'],
            'username' => $data['username'],
            'passwords' => $data['passwords'],
            'provinsi' => $data['provinsi'],
            'kota' => $data['kota'],
            'alamat' => $data['alamat']
        );
    }

    $response = array(
        'status' => 'OK',
        'data' => $items
    );

    echo json_encode($response);
?>