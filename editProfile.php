<?php
    require('users.php');
    require('users_info.php');
    require('update_profile.php');

    if (!isset($_SESSION['login'])) {
        header("Location: login.php");
        exit;
    }
?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <link rel=”icon” href=”img/ic_web.png”>
        <title>Edit Profile</title>
    
        <link rel="stylesheet" href="bootstrap-5.0.2-dist/css/bootstrap.min.css">
        <link rel="stylesheet" href="css/style_editProfile.css">
    
        <!--font-->
        <style>
            @import url('https://fonts.googleapis.com/css2?family=Poppins:wght@400;500;600;700&display=swap');
            @import url('https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;500;700&display=swap');
        </style>
    </head>
    <body>
        <nav class="navbar navbar-expand-lg navbar-light shadow">
            <div class="container">
                <a class="navbar-brand fw-bolder" href="main.php">
                    <img src="img/ic_web.png" alt="" width="40" height="40" style="margin-right: 10px; ">EST Cameras
                </a>
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse justify-content-between text-center" id="navbarNavAltMarkup">
                    <div class="navbar-nav ms-auto nav-barbar">
                        <h6 class="d-flex justify-content-center align-items-center"><?php echo $row['nama'];?></h6>
                        <div class="dropdown">
                            <button class="btn dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                            <?php if ($row['gambar'] == "") : ?>
                                <img class="img-user" style="width:35px; height:35px; border-radius:50%; object-fit:cover;" src="img/ic_user.png" class="img-user" alt="" class="dropdown-toggle" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                            <?php else : ?>
                                <img class="img-user" style="width:35px; height:35px; border-radius:50%; object-fit:cover;" src="uploads/<?php echo $row['gambar'];?>" class="img-user" alt="" class="dropdown-toggle" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                            <?php endif;?>
                            </button>
                            <ul class="dropdown-menu p-4 font-poppins-reg" aria-labelledby="dropdownMenuButton1">
                              <li><a class="dropdown-item" href="#">Edit Profile</a></li>
                              <li><a class="dropdown-item" href="inputData.php">Sewakan Kamera</a></li>
                              <li><a class="dropdown-item" href="#">Menunggu Konfirmasi</a></li>
                              <li><a class="dropdown-item" href="#">Pelanggan Saya</a></li>
                              <li><a class="dropdown-item btn-logout-dwn" href="logout.php">Log Out</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </nav>
        <div class="container cont mt-5">
            <form action="" method="post" enctype="multipart/form-data">
                <h2 class="mb-5 font-poppins-semi">Edit Profile</h2>
                <div class="row pb-4">
                    <div class="col-3 me-4">
                        <div class="pic-profile">
                            <input type="file" id="image_input" name="input_img" accept="image/png, image/jpg">
                            <label for="image_input" id="uploadBtn">pilih foto</label>
                            <?php if ($row['gambar'] == "") : ?>
                                <img src="img/no_image.jpg" id="photo">
                            <?php else : ?>
                                <img src="uploads/<?php echo $row['gambar'];?>" id="photo">
                            <?php endif;?>
                        </div>
                    </div>
                    <div class="col d-flex align-items-center">
                        <h1 class="font-poppins-med"><?php echo $row['nama'];?></h1>
                    </div>
                </div>
                <hr>
                <div class="row pt-3 font-poppins-reg">
                    <div class="col">
                        <h3>Informasi</h3>
                        <p>Anda dapat mengubah Email <br> dan no telepon</p>
                    </div>
                    <div class="col">
                        <div class="mb-3">
                            <label for="kodeBuku" class="form-label">Email</label>
                            <input type="text" class="form-control" id="kodeBuku" name="email" value="<?php echo $row['email'];?>">
                        </div>
                        <div class="mb-5">
                            <label for="kodeBuku" class="form-label">No Telepon</label>
                            <input type="text" class="form-control" id="kodeBuku" name="notlpn" value="<?php echo $row['no_telepon']; ?>">
                        </div>
                    </div>
                </div>
                <hr>
                <div class="row pt-3 row pt-3 font-poppins-reg">
                    <div class="col">
                        <h3>Alamat</h3>
                        <p>Informasi mengenai alamat Anda agar mudah dikenali</p>
                    </div>
                    <div class="col">
                        <div class="mb-3">
                            <label for="kodeBuku" class="form-label">Kota</label>
                            <input type="text" class="form-control" id="kodeBuku" name="kota" value="<?php echo $row['kota']; ?>">
                        </div>
                        <div class="mb-3">
                            <label for="kodeBuku" class="form-label">Provinsi</label>
                            <input type="text" class="form-control" id="kodeBuku" name="prov" value="<?php echo $row['provinsi']; ?>">
                        </div>
                        <div class="mb-5">
                            <label for="exampleFormControlTextarea1" class="form-label">Detail Alamat</label>
                            <textarea class="form-control" id="exampleFormControlTextarea1" rows="6" name="alamat"><?php echo $row['alamat']; ?></textarea>
                        </div>
                    </div>
                </div>
                <hr>
                <div class="row pt-3 row pt-3 font-poppins-reg mb-5">
                    <div class="col">
                        <h3>Password</h3>
                        <p>Anda dapat merubah atau memperbarui password Anda</p>
                    </div>
                    <div class="col">
                        <div class="mb-3">
                            <label for="kodeBuku" class="form-label">Password Baru</label>
                            <input type="text" class="form-control" id="kodeBuku" name="newpass">
                        </div>
                    </div>
                </div>
                <button type="submit" class="btn btn-dark mt-4 mb-5" name="update">Simpan</button>
            </form>
        </div>
        <script type="text/javascript" src="bootstrap-5.0.2-dist/js/bootstrap.min.js"></script>
        <script type="text/javascript">
            const imgDiv = document.querySelector('.pic-profile');
            const img = document.querySelector('#photo');
            const file = document.querySelector('#image_input');
            const uploadBtn = document.querySelector('#uploadBtn');

            imgDiv.addEventListener('mouseenter', function(){
                uploadBtn.style.display = "block";
            });

            imgDiv.addEventListener('mouseleave', function(){
                uploadBtn.style.display = "none";
            });

            file.addEventListener('change', function(){
                const choosedFile = this.files[0];

                if (choosedFile) {
                    const reader = new FileReader();

                    reader.addEventListener('load', function(){
                        img.setAttribute('src', reader.result);
                    });

                    reader.readAsDataURL(choosedFile);
                }
            });
        </script>
    </body>
</html>