<?php
    if (isset($_POST['update'])) {
        $up_email = $_POST['email'];
        $up_notlpn = $_POST['notlpn'];
        $up_kota = $_POST['kota'];
        $up_provinsi = $_POST['prov'];
        $up_alamat = $_POST['alamat'];
        $up_newpass = $_POST['newpass'];

        //upload gambar
        $gambar = upload();
        $sername = $_SESSION['username_user'];


        if ($up_newpass != "") {

            $up_newpass = password_hash($up_newpass, PASSWORD_DEFAULT);

            mysqli_query($conn, "UPDATE users SET no_telepon='$up_notlpn', email='$up_email', passwords='$up_newpass', provinsi='$up_provinsi', kota='$up_kota', alamat='$up_alamat' WHERE username='$sername'");

            header("Refresh:0");
        } else if($up_newpass != "" && !$gambar) {
            $up_newpass = password_hash($up_newpass, PASSWORD_DEFAULT);
            mysqli_query($conn, "UPDATE users SET gambar='$gambar', no_telepon='$up_notlpn', email='$up_email', passwords='$up_newpass', provinsi='$up_provinsi', kota='$up_kota', alamat='$up_alamat' WHERE username='$sername'");

            header("Refresh:0");
        } else if($up_newpass == "" && $gambar) {
            mysqli_query($conn, "UPDATE users SET gambar='$gambar', no_telepon='$up_notlpn', email='$up_email', provinsi='$up_provinsi', kota='$up_kota', alamat='$up_alamat' WHERE username='$sername'");
            
            header("Refresh:0");
        }
        
    }

    function upload() {
        $namaFile = $_FILES['input_img']['name'];
        $ukuranFile = $_FILES['input_img']['size'];
        $error = $_FILES['input_img']['error'];
        $tmpName = $_FILES['input_img']['tmp_name'];

        $ektensiGambarValid = ['jpg', 'jpeg', 'png'];
        $ektensiGambar = explode('.', $namaFile);
        $ektensiGambar = strtolower(end($ektensiGambar));

        if ( $ukuranFile > 1048576) {
            echo "<script>alert('Ukuran gambar terlalu besar!'); document.location.href = 'editProfile.php';</script>";
            return false;
        }

        //Buat nama file baru
        $namaFileBaru = uniqid();
        $namaFileBaru .= '.';
        $namaFileBaru .= $ektensiGambar;

        move_uploaded_file($tmpName, 'uploads/' . $namaFileBaru);
        return $namaFileBaru;
    }
?>