<?php
    require('users.php');
    require('users_info.php');

    if (!isset($_SESSION['login'])) {
        header("Location: login.php");
        exit;
    }

    $id = $_GET['id'];
    

    $get_data_camera = mysqli_query($conn, "SELECT * FROM data_kamera WHERE id='$id'");
    $rowValueData = mysqli_fetch_assoc($get_data_camera);

    if(isset($_POST['hapus'])){
        mysqli_query($conn, "DELETE FROM data_kamera WHERE id='$id'");
        
        header("Location: main.php");
    }

    if(isset($_POST['update'])) {
        $namaKamera = $_POST['nama_kamera'];
        $makshari = $_POST['hari'];
        $spesifikasi = $_POST['spesifikasi'];
        $harga = $_POST['harga'];

        $gambar = upload();

        $ex = explode('.', $gambar);

        if ($ex[1] == 'png' || $ex[1] == 'jpg' || $ex[1] == 'jpeg'){
            mysqli_query($conn, "UPDATE data_kamera SET gambar='$gambar', nama_kamera='$namaKamera', max_sewa='$makshari', spesifikasi='$spesifikasi', harga='$harga' WHERE id='$id'");
            
        } else {
            mysqli_query($conn, "UPDATE data_kamera SET nama_kamera='$namaKamera', max_sewa='$makshari', spesifikasi='$spesifikasi', harga='$harga' WHERE id='$id'");
        }
        

        if(mysqli_affected_rows($conn) > 0){
            echo "<script>alert('Berhsil diupdate');</script>";
        }

        header("Location: main.php");
    }

    function upload() {
        $namaFile = $_FILES['input_img']['name'];
        $ukuranFile = $_FILES['input_img']['size'];
        $error = $_FILES['input_img']['error'];
        $tmpName = $_FILES['input_img']['tmp_name'];

        $ektensiGambarValid = ['jpg', 'jpeg', 'png'];
        $ektensiGambar = explode('.', $namaFile);
        $ektensiGambar = strtolower(end($ektensiGambar));

        if ( $ukuranFile > 1048576) {
            echo "<script>alert('Ukuran gambar terlalu besar!'); document.location.href = 'editProfile.php';</script>";
            return false;
        }

        //Buat nama file baru
        $namaFileBaru = uniqid();
        $namaFileBaru .= '.';
        $namaFileBaru .= $ektensiGambar;

        move_uploaded_file($tmpName, 'uploads/kamera/' . $namaFileBaru);
        return $namaFileBaru;
    }
?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Document</title>
    
        <link rel="stylesheet" href="bootstrap-5.0.2-dist/css/bootstrap.min.css">
        <link rel="stylesheet" href="css/style_editData.css">
    
        <!--font-->
        <style>
            @import url('https://fonts.googleapis.com/css2?family=Poppins:wght@400;500;600;700&display=swap');
            @import url('https://fonts.googleapis.com/css2?family=Roboto:wght@300;400;500;700&display=swap');
        </style>
    </head>
    <body>
        <nav class="navbar navbar-expand-lg navbar-light shadow">
            <div class="container">
                <a class="navbar-brand fw-bolder" href="main.php">
                    <img src="img/ic_web.png" alt="" width="40" height="40" style="margin-right: 10px; ">EST Cameras
                </a>
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse justify-content-between text-center" id="navbarNavAltMarkup">
                    <div class="navbar-nav ms-auto nav-barbar">
                        <h6 class="justify-content-center align-self-center"><?php echo $row['nama'];?></h6>
                        <div class="dropdown">
                            <button class="btn dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                            <?php if ($row['gambar'] == "") : ?>
                                <img class="img-user" style="width:35px; height:35px; border-radius:50%; object-fit:cover;" src="img/ic_user.png" class="img-user" alt="" class="dropdown-toggle" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                            <?php else : ?>
                                <img class="img-user" style="width:35px; height:35px; border-radius:50%; object-fit:cover;" src="uploads/<?php echo $row['gambar'];?>" class="img-user" alt="" class="dropdown-toggle" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                            <?php endif;?>
                            </button>
                            <ul class="dropdown-menu p-4 font-poppins-reg" aria-labelledby="dropdownMenuButton1">
                              <li><a class="dropdown-item" href="editProfile.php">Edit Profile</a></li>
                              <li><a class="dropdown-item" href="inputData.php">Sewakan Kamera</a></li>
                              <li><a class="dropdown-item" href="menungguKonfirmasi.php">Menunggu Konfirmasi</a></li>
                              <li><a class="dropdown-item" href="pelangganSaya.php">Pelanggan Saya</a></li>
                              <li><a class="dropdown-item btn-logout-dwn" href="logout.php">Log Out</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </nav>
        <form action="" method="post" enctype="multipart/form-data">
            <div class="container cont mt-5">
                <h2 class="mb-5">Edit Data Sewa Kamera</h2>
                <div class="row pb-4">
                    <div class="col">
                        <p style="display: none;" id="getGambar"><?php echo $rowValueData['gambar'];?></p>
                        <div id="display_images">
                            <img src="uploads/kamera/<?php echo $rowValueData['gambar'];?>" id="photo" alt="" srcset="" style="width: 100%; height: 100%; object-fit:cover;">
                        </div>
                    </div>
                    <div class="col d-flex align-items-center">
                        <input type="file" id="image_input" name="input_img" accept="image/png, image/jpg">
                    </div>
                </div>
                <hr>
                <div class="row pt-3">
                    <div class="col">
                        <h3>Informasi</h3>
                        <p>Anda dapat mengubah Email dan no telepon</p>
                    </div>
                    <div class="col">
                        <div class="mb-3">
                            <label for="kodeBuku" class="form-label">Nama Kamera</label>
                            <input type="text" class="form-control" id="kodeBuku" name="nama_kamera" value="<?php echo $rowValueData['nama_kamera'];?>">
                        </div>
                        <div class="mb-3">
                            <label for="kodeBuku" class="form-label">Harga Sewa Perhari</label>
                            <input type="text" class="form-control" id="kodeBuku" name="harga" value="<?php echo $rowValueData['harga'];?>">
                        </div>
                        <div class="mb-3">
                            <label for="kodeBuku" class="form-label">Maks. Sewa (hari)</label>
                            <input type="text" class="form-control" id="kodeBuku" name="hari" value="<?php echo $rowValueData['max_sewa'];?>">
                        </div>
                        <div class="mb-3">
                            <label for="exampleFormControlTextarea1" class="form-label">Spesifikasi / Deskripsi</label>
                            <textarea class="form-control" id="exampleFormControlTextarea1" rows="6" name="spesifikasi"><?php echo $rowValueData['spesifikasi'];?></textarea>
                        </div>
                    </div>
                </div>
                <div class="row mt-5">
                    <div class="col">
                        <button type="submit" class="btn btn-card-view btn-danger mb-4" name="hapus">Hapus</button>
                    </div>
                    <div class="col">
                        <button type="submit" class="btn btn-card-view btn-dark mb-4" name="update">Update</button>
                    </div>
                </div>
            </div>
        </form>
        <script type="text/javascript" src="bootstrap-5.0.2-dist/js/bootstrap.min.js"></script>
        <script type="text/javascript">
            const image_input = document.querySelector("#image_input");
            const img = document.querySelector("#photo");

            image_input.addEventListener('change', function(){
                const choosedFile = this.files[0];

                if (choosedFile) {
                    const reader = new FileReader();

                    reader.addEventListener('load', function(){
                        img.setAttribute('src', reader.result);
                    });

                    reader.readAsDataURL(choosedFile);
                }
            });
        </script>
    </body>
</html>